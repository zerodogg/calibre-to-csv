#!/usr/bin/env raku
# calibre-to-csv - Build a CSV of book data from a calibre database
# Copyright (C) Eskild Hustvedt 2022
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
use DBIish;
use DBDish::SQLite::Connection;

role CTCMessages
{
    has $.verbosity = 0;

    #| Output a message in verbose mode
    method sayv(Str $message)
    {
        if $.verbosity > 0
        {
            say $message;
        }
    }
}

class CalibreInterface does CTCMessages
{
    #| The path to the DB file
    has Str $.calibre-db-file is required;
    #| The connection to sqlite
    has DBDish::SQLite::Connection $!dbh;

    #| Retrieves the database handle. Will connect if needed.
    method dbh()
    {
        if !$!dbh.defined
        {
            $!dbh = DBIish.connect("SQLite", :database($.calibre-db-file));
        }
        return $!dbh;
    }

    #| Converts a tag name (string) into an ID
    method getTagIDByName(Str $tag)
    {
        my $sth = self.dbh.prepare('SELECT * FROM tags WHERE name = ?');
        my @rows = $sth.execute($tag).allrows(:array-of-hash);
        if @rows.elems > 1
        {
            die "Found several tags named $tag, this should be impossible";
        }
        if @rows.elems == 0
        {
            die "Found no tags named $tag";
        }
        return @rows[0]<id>;
    }

    #| Retrieves a list of authors for the given book ID, as a string
    method authorsForBookID(Int $id)
    {
        my $sth = self.dbh.prepare('SELECT name FROM authors LEFT OUTER JOIN books_authors_link ON authors.id = books_authors_link.author WHERE books_authors_link.book = ?');
        my @rows = $sth.execute($id).allrows(:array);
        my $author = @rows[0].join(', ');
        if $author.contains('|')
        {
            $author = $author.split('|').join(',');
        }
        return $author;
    }

    #| Retrieves the publisher(s) for the given book ID, as a string, or Nil
    method publishersForBookID(Int $id)
    {
        my $sth = self.dbh.prepare('SELECT name FROM publishers LEFT OUTER JOIN books_publishers_link ON publishers.id = books_publishers_link.publisher WHERE books_publishers_link.book = ?');
        my @rows = $sth.execute($id).allrows(:array);
        if @rows.elems > 0
        {
            my $publisher = @rows[0].join(', ');
            return $publisher;
        }
    }

    #| Given a %book hash, adds an authors entry based on the author or publisher
    method constructBookInfo(%book is copy) returns Hash
    {
        my $authors = self.authorsForBookID(%book<book>);
        if $authors eq 'Unknown' && %book<author_sort> ne 'Unknown'
        {
            $authors = %book<author_sort>;
        }
        elsif $authors eq 'Unknown'
        {
            my $publisher = self.publishersForBookID(%book<book>);
            if $publisher ne '' && $publisher ne 'Unknown'
            {
                $authors = $publisher;
            }
        }
        %book<authors> = $authors;
        my $isbn = self.getISBNFromID(%book<book>);
        if $isbn.defined
        {
            %book<isbn> = $isbn;
        }
        return %book;
    }

    #| Given a book ID, returns the ISBN if known
    method getISBNFromID(Int $id)
    {
        my $sth = self.dbh.prepare('SELECT * FROM identifiers WHERE type = \'isbn\' AND book = ?');
        my @rows = $sth.execute($id).allrows(:array-of-hash);
        if @rows.elems != 1
        {
            return;
        }
        return @rows[0]<val>;
    }

    #| Retrieves all books with the given tag
    method getByTag(Str $tag) returns Array
    {
        my $tagID = self.getTagIDByName($tag);
        my $sth = self.dbh.prepare('SELECT * FROM books LEFT OUTER JOIN books_tags_link ON books_tags_link.book = books.id WHERE books_tags_link.tag = ?');
        my @rows = $sth.execute($tagID).allrows(:array-of-hash);
        my @result;
        for @rows -> %entry
        {
            @result.push: self.constructBookInfo(%entry);
        }
        return @result;
    }

    #| Checks if the given book ID has any of the tags in @tags (array of tag IDs)
    method bookHasAnyTagFromList($bookID, @tags)
    {
        if !@tags.defined || @tags.elems == 0
        {
            return False;
        }
        my $sth = self.dbh.prepare("SELECT COUNT(id) FROM books_tags_link WHERE book = ? AND tag IN ("~@tags.join(",")~')');
        my $query = $sth.execute($bookID);
        my @row = $query.row();
        return @row[0] > 0;
    }

    #| Retrieves all books, except those tagged as @except (array of tag strings)
    method getAll(@except)
    {
        my @exceptTagIDs;
        for @except -> $tagName
        {
            my $tagID = self.getTagIDByName($tagName);
            @exceptTagIDs.push: $tagID;
        }
        my $query = 'SELECT *, id as book FROM books';
        my $sth = self.dbh.prepare($query);
        my @rows = $sth.execute().allrows(:array-of-hash);
        my @result;
        for @rows -> %entry
        {
            if self.bookHasAnyTagFromList(%entry<book>,@exceptTagIDs)
            {
                next;
            }
            @result.push: self.constructBookInfo(%entry);
        }
        return @result;
    }

    #| Retrieves the name of the series for bookID
    method getSeriesNameForBook(Int $bookID)
    {
        my $sth = self.dbh.prepare('SELECT name FROM series LEFT OUTER JOIN books_series_link ON series.id = books_series_link.series WHERE books_series_link.book = ?');
        my @rows = $sth.execute($bookID).allrows(:array);
        if @rows.elems == 0
        {
            return;
        }
        if @rows.elems > 1
        {
            die "Found more than one series for book with ID $bookID";
        }
        my $series = @rows[0].join(', ');
        return $series;
    }
}

class BooksToCSV is CalibreInterface
{
    has $.mergeVolumes = False;
    has $.include-volumes = False;
    has $.include-isbn = False;
    has $.skip-incomplete = False;
    has $.overwrite = False;
    has $.output-file;

    #| Extract the volume ID from the given text title, or Nil
    method !extractVolumeName($text)
    {
        my $volNo = $text;
        $volNo ~~ s:i{.*[vol[ume]?|issue|\#]\.?\s*(\d+)\D+} = $0;
        $volNo ~~ s:i{.*\D(\d+)$} = $0;
        if $volNo ne $text
        {
            return $volNo.Int;
        }
        return;
    }

    #| Remove volume identifier from the given text title
    method !removeVolumeName($text)
    {
        my $filtered = $text;
        $filtered ~~ s:i{\s*[vol[ume]?|issue|\#]\.?\s*\d+\.?} = "";
        $filtered ~~ s{\s*\d+$} = "";
        $filtered ~~ s{[\,]+$} = "";
        $filtered ~~ s:g{",:"} = ":";
        return $filtered;
    }

    #| Retrieve the volume number for the given %book, either from metadata, or extracted from the title.
    method !getVolumeName(%book)
    {
        my $volume = %book<series_index>.Int;
        if !$volume.defined || $volume == 1
        {
            my $extracted = self!extractVolumeName(%book<title>);
            if $extracted.defined
            {
                $volume = $extracted;
            }
            else
            {
                return 1;
            }
        }
        return $volume;
    }

    #| Retrieve the series name, either from metadata or generated from the title
    method !getSeriesName(%book)
    {
        my $series = self.getSeriesNameForBook(%book<book>);
        if $series.defined
        {
            return $series;
        }
        return self!removeVolumeName(%book<title>);
    }

    #| Merge multiple volumes of a series into a single entry in @list. Returns a modified @list.
    method mergeVolumesIntoOne(@list)
    {
        my %books;
        for @list -> %book
        {
            my $name = self!getSeriesName(%book);
            my $volume = self!getVolumeName(%book);
            %books{$name} //= %book;
            %books{$name}<title> = $name;
            %books{$name}<issues> //= [];
            %books{$name}<issues>.push: $volume;
            if %books{$name}<authors> eq '' || %books{$name}<authors> eq 'Unknown'
            {
                %books{$name}<authors> = %book<authors>;
            }
        }
        my @result;
        for %books.kv -> $key,%value
        {
            if %value<issues>.defined
            {
                %value<issues> = %value<issues>.sort.List;
            }
            @result.push: %value;
        }
        return @result;
    }

    #| Check if a given %book is incomplete
    method isIncomplete(%book) returns Bool
    {
        if %book<title>.starts-with('/')
        {
            self.sayv("Skipping %book<title> because it starts with /");
            return True;
        }
        if %book<authors>.contains('Unknown')
        {
            self.sayv("Skipping %book<title> because its author is Unknown");
            return True;
        }
        return False;
    }

    #| Hacky CSV sanitizer
    method !sanitizeForCSV($title is copy)
    {
        $title ~~ s:g{";"} = ",";
        $title ~~ s:g{\"} = "";
        return $title;
    }

    #| Given a list of books, generate a CSV and either write it to a file or STDOUT
    method !resultToCSV(@entries)
    {
        if $.mergeVolumes
        {
            @entries = self.mergeVolumesIntoOne(@entries);
        }
        my @csv;
        my @header = ('Title','Author');
        if $.include-volumes
        {
            @header.push('Volumes');
        }
        if $.include-isbn
        {
            @header.push('ISBN');
        }
        for @entries -> %entry
        {
            if $.skip-incomplete
            {
                next if self.isIncomplete(%entry);
            }
            my @line = (self!sanitizeForCSV(%entry<title>), self!sanitizeForCSV(%entry<authors>));
            if $.include-volumes
            {
                my $volumes = "";
                if %entry<issues>.elems > 0
                {
                    $volumes = %entry<issues>.join(', ');
                }
                @line.push: $volumes;
            }
            if $.include-isbn
            {
                my $isbn = "";
                if %entry<isbn>.defined
                {
                    $isbn = %entry<isbn>;
                }
                @line.push: $isbn;
            }
            @csv.push: @line.join(';');
        }
        @csv = @csv.sort.List;
        if $.output-file.defined
        {
            if $.output-file.IO.e && !$.overwrite
            {
                die "$.output-file: already exists";
            }
            $.output-file.IO.spurt(
                @header.join(";")~"\n"~
                @csv.join("\n")
            );
            say "Wrote "~(@csv.elems-1)~" entries to $.output-file";
        }
        else
        {
            say
                @header.join(";")~"\n"~
                @csv.join("\n");
        }
    }

    #| Generate CSV for a given tag
    method tagToCSV($tag)
    {
        my @entries = self.getByTag($tag);
        if @entries.elems == 0
        {
            die "Got no entries from getByTag";
        }
        return self!resultToCSV(@entries);
    }

    #| Generate CSV for all books except those tagged @except (array of tag strings)
    method allToCSV(@except)
    {
        my @entries = self.getAll(@except);
        if @entries.elems == 0
        {
            die "Got no entries from getAll";
        }
        return self!resultToCSV(@entries);
    }
}

sub MAIN(Str :$calibre-db-file is required, #= The path to the database file.
    Str :$category,                         #= Limit output to this category.
    :@except-category,                      #= Exclude this category when outputting, may be provided multiple times.
    Str :$output-file,                      #= Output to this file instead of STDOUT.
    Bool :$overwrite = False,               #= Overwrite --output-file if it already exists.
    Bool :$include-isbn = False,            #= Include 'ISBN' as a column.
    Bool :$include-volumes is copy = False, #= Include 'volumes' as a column.
    Bool :$merge-volumes = False,           #= Merge multi-volume entries into a single entry. Implies --include-volumes.
    Bool :$skip-incomplete = False,         #= Skip incomplete books, ie. those without author.
    Bool :$verbose = False,                 #= Be more verbose.
    )
{
    my $verbosity = 0;
    if $verbose
    {
        $verbosity = 1;
    }
    if $merge-volumes
    {
        $include-volumes = True;
    }
    my $calibre = BooksToCSV.new(
        :$calibre-db-file,
        :$include-volumes,
        :$include-isbn,
        :mergeVolumes($merge-volumes),
        :$skip-incomplete,
        :$verbosity,
        :$output-file,
        :$overwrite
    );
    if $category.defined && @except-category.elems > 0
    {
        die "--category and --except-category can not be combined";
    }
    if $category.defined
    {
        $calibre.tagToCSV($category);
    }
    else
    {
        $calibre.allToCSV(@except-category);
    }
}
