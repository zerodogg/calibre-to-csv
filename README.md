# calibre-to-csv

A small program that can generate CSV-files from a Calibre database.

## Usage

```
  calibre-to-csv --calibre-db-file=<Str> [--category=<Str>] [--except-category=<Any> ...] [--output-file=<Str>] [--overwrite] [--include-isbn] [--include-volumes] [--merge-volumes] [--skip-incomplete] [--verbose]

    --calibre-db-file=<Str>        The path to the database file.
    --category=<Str>               Limit output to this category.
    --except-category=<Any> ...    Exclude this category when outputting, may be provided multiple times.
    --output-file=<Str>            Output to this file instead of STDOUT.
    --overwrite                    Overwrite --output-file if it already exists. [default: False]
    --include-isbn                 Include 'ISBN' as a column. [default: False]
    --include-volumes              Include 'volumes' as a column. [default: False]
    --merge-volumes                Merge multi-volume entries into a single entry. Implies --include-volumes. [default: False]
    --skip-incomplete              Skip incomplete books, ie. those without author. [default: False]
    --verbose                      Be more verbose. [default: False]
```

## Example

`calibre-to-csv --calibre-db-file ~/E-books/Calibre/metadata.db --skip-incomplete --overwrite --category Manga --merge-volumes --output-file Manga.csv`  
Extract all books tagged "Manga", skipping incomplete entries and merging
multiple volumes into a single entry, writing the CSV to the file Manga.csv,
overwriting it if it currently exists.

## Installation

`calibre-to-csv` is written in [Raku](https://raku.org). If your distribution
does not have up-to-date raku packages you can find repositories for most
distributions (Fedora, Debian, Ubuntu, ..) at
[rakudo-pkg](https://github.com/nxadm/rakudo-pkg#os-repositories).  Once you
have raku installed, you can install the modules that calibre-to-csv uses by
running `zef install --deps-only .`. Once you have done that, you can run
`./calibre-to-csv`. If you want to install it into your PATH you can do `zef
install .`.

### Installation troubleshooting

**zef is missing (using rakudo-pkg)**  
Run `/opt/rakudo-pkg/bin/install-zef`.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
